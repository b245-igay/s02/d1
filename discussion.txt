Git Demonstration

GENERATE SSH KEYS AND CONFIGURE MACHINE (One time setup/for new machines)
-GENERATE SSH KEY (ssh-keygen) press enter key 3 times
NOTE: If .ssh folder not generated, try recreating it again.
-TO COPY SSH KEY:
Linux
  xclip -sel clip < ~/.ssh/id_rsa.pub

Mac:
  pbcopy < ~/.ssh/id_rsa.pub

Windows:
  cat ~/.ssh/id_rsa.pub | clip

-ADD SSH KEYS TO GITLAB (profile>>edit profile>>ssh keys>>paste SSH Key>>Add key)
-GLOBAL USER EMAIL CONFIG (git config --global user.email )
-GLOBAL USERNAME CONFIG (git config --global user.name )
-SHOW GLOBAL CREDENTIAL (git config --global --list)

TERMINAL COMMANDS FOR CREATING FOLDER AND TRAVERSING DIRECTORIES:
1. cd - change the directory or accessing other folders.
	example:
		cd documents/
    cd .. (for one folder up)
2. mkdir - to create a folder
	example:
		mkdir s02

3. ls - to list all the files/folders in a directory

4. touch - to create a file
	example:
		touch discussion.txt (Note: Make sure that proper filename and extension in creating a file)

GIT BASICS COMMAND:

===== For preparation and creating a local git repository =====

Note: Make sure that you are in the correct directory of your project.

1. git init
 - This command is used to prepare and set your local repository.
 - A .git folder will be created inside the current directory.
 - Note: You will not be able to see this folder because it is set to hidden. 
 - rm -rf .git - command used to remove git initialization or initialized a wrong directory.

2. git status command
 - This command will display all updates that are not yet saved to the latest commit version of the project.
 - Lets you see which changes have been staged, which haven't, and which files aren't being tracked.

3. git add [filename] command
 - this command will "add" or "stage" the files preparing them to be included in the next commit/snapshot of the project.
 - Prepares a file or set of files into the staging area, where the files are eventually added to the recorded history of changes.
 - Example:
 			git add discussion.txt

4. git add . / git add -A command
- To stage all files in the local repository
- Note: Adding all files is normal practice to make sure all files that were updated would be included in the next commit. Individually staging files is useful for minor revisions.

5. git commit -m “[message]” command

 - Saves the changes added from git add to its recorded history of changes or commits.
 - Example:
 		git commit -m "Initial Commit"
 - "initial commit" is used in the message to help developers identify the first commit.
 - Succeeding commit messages should be descriptive of the changes to the project.

NOTE:
Writing a good commit
- Capitalize the first letter in the message.
- Limit the message to 50 characters or less.
- Do not end the commit message with a period.
- Adding a punctuation is unnecessary.
- Use the imperative mood in the subject line.

6. git log command
 - Displays the history of changes for the repository.
 - git log --oneline will simplify the display output.

 ===== For connecting local git repository to a remote git repository =====

 Note: Make sure that you have already created your repository in gitlab

 Create a project repo in gitlab
   1. Go to your batch folder group.
   
   2. Inside the batch folder, Create a new subgroup for the current session (make sure visibility level is set to public).

   3. Inside the session subgroup, create a new project with the following settings:
    - Visibility Level: Public
    - Uncheck two items of project configuration

7. git remote add [remote-name] [git-repository-link]
 - this command will add a reference to a git project.
 - Example:
 		git remote add origin git@gitlab.com:b182/s02/d1.git
 - "origin" is mostly used to help developers identify the main remote repository linked to the project.
 - Note: Multiple remote repositories can be added to a project.

 8. git remote -v
 - This command is used to check the remote name and url.

 9. git push [remote-name] [branch-name]
 - this command will "push" the changes from the local repository to the remote repository.
 - Example:
 		git push origin master
 - "push" is a term used for uploading the local repository to the remote repository.
 - "master" refers to a branch or a snapshot of your project.
 - This command is commonly used because "origin" is considered as the main repository for a project.

NOTE: Always check if the files are stage and commited before pushing to avoid any errors.

  ===== Other git commands for a certain situation =====

 10. git remote remove [remote-name]
 - This command is used to remove a remote repository connected to the local repository.
 - Example:
 		git remote remove origin
 - Note: This is used if we mistakenly included a wrong link and name in our local repository or if you have clone a git repository.

 11. git remote get-url [remote-name]
 - Get the url of a remote repository.
 - Example:
      git remote get-url origin

 12. git remote set-url [remote-name] [git-repository-link]
 - Change the url of a remote name.
 - Example:
      GitLab
        git remote set-url secondary git@gitlab.com:johndoe/test.git 

 13. git clone [git-ssh-repository-link]
 - This command will download your git project into the current directory the terminal is located at.
 - "clone" is a term used for downloading the remote repository to a device and creating a copy of the project.
 - Example:
		git clone git@gitlab.com:b182/s02/d1.git
